#include<tree.h>

BST * allocNode(int data)
{
								BST * temp = new BST;
								temp->left = temp->right = NULL;
								temp->data = data;
								return temp; 
}

BST * InsertBSTNode (BST * root, int data) {
								cout<<"In "<<__FUNCTION__<<endl;
								BST * temp = allocNode(data); 
								if(root == NULL)
																return temp;
								BST * node = root; 
								BST * prev = root;

								while(node) {
																prev = node;
																if(data < node->data) 
																								node = node->left;
																elseif(data > node->data) 
																								node = node->right;
								}

       if(prev->data < data)
           prev->left = temp;
       else
           prev->right = temp;

								return root; 
}

BST * InsertBSTNodeRecursive(BST * root, int data){
								if(root == NULL) {
																return allocNode(data);
								} else if(data <= root->data) 
																root->left = InsertBSTNodeRecursive(root->left, data);
								else
																root->right = InsertBSTNodeRecursive(root->right, data);
								return root;
								//TBD: Check the return common for all cases
}

void PrintBSTBreadthFirst(BST * root) {
								queue<BST *> Q;
								if(root == NULL)
																return;
								Q.push(root);
        int levelcount =1;
								while(!Q.empty()) {
               levelcount = Q.size();
              while(levelcount > 0) {
               BST * temp = Q.front();
             			cout<<" "<<Q.front();
																if(temp->left)
																								Q.push(temp->left);
																if(temp->right)
																								Q.push(temp->right);

                     levelcount--;
																     Q.pop();
                       
                }   
                printf("\n");
								}
}

//Height Using iterative:http://www.geeksforgeeks.org/iterative-method-to-find-height-of-binary-tree/
int FindHeightIterativ(BST * root) {
        int height = 0;
								queue<BST *> Q;
								if(root == NULL) {
																	return height;
        }
								Q.push(root);
        int levelcount =1;
								while(!Q.empty()) {
               levelcount = Q.size();
              while(levelcount > 0) {
               BST * temp = Q.front();
																cout<<" "<<temp->data<<" ";
																if(temp->left)
																								Q.push(temp->left);
																if(temp->right)
																								Q.push(temp->right);

                     levelcount--;
																     Q.pop();
                       
                }   
                height++;
                cout<<endl;
								}
}



void TraverseBSTBreadthFirst(BST * root) {
								queue<BST *> Q;
								if(root == NULL)
																return;
								Q.push(root);

								while(!Q.empty()) {
               BST * temp = Q.front();
																cout<<" "<<temp->data<<" ";
																if(root->left)
																								Q.push(root->left);
																if(root->right)
																								Q.push(root->right);

																Q.pop();
								}
								cout<<endl;
}

BST * deleteBSTNode(BST * root, int data) {

								if (root == NULL)
																return root;

								if(data <= root->data)
																root->left = deleteBSTNode(root->left);
								else if(data > root->data)
																root->right = deleteBSTNode(root->right);
								else {

																if(root->left == NULL && root->right == NULL ) {
																								delete root;
																								root =NULL;
																}
																else if(root->left == NULL && root->right != NULL ) {
																								BST * temp = root;
																								root = root->right;
																								delete temp;
																}
																else if(root->left != NULL && root->right == NULL ) {
																								BST * temp = root;
																								root = root->left;
																								delete temp;
																}else {
																								BST * temp = FindMin(root->right)
																																root->data = temp->data;
																								root->right = deleteBSTNode(root->right,temp->data);
																}
																return root;
								}


}

bool searchBSTNode(BST * root, int data) {

								while(root) {
																if(data  == root->data)
																								return true;
																else if(data < root->data)
																								root = root->left;
																else
																								root = root->right;
								} 
								return false;
}

bool searchBSTNodeRecursive(BST * root, int data) {
								if(root == NULL)
																return false;
								else if(data  == root->data)
																return true;
								else if(data < root->data)
																return searchBSTNodeRecursive(root->left,data);
								else 
																return searchBSTNodeRecursive(root->right,data);
}


void TraverseBSTPreOrderRecursive(BST * root) {
								if(root == NULL)   
																return;
								cout<<root->data<<"\t";
								TraverseBSTPreOrderRecursive(root->left);
								TraverseBSTPreOrderRecursive(root->right); 
}

void TraverseBSTPostOrderRecursive(BST * root) {
								if(root == NULL)   
																return;
								TraverseBSTPostOrderRecursive(root->left);
								TraverseBSTPostOrderRecursive(root->right); 
								cout<<root->data<<"\t";
}

void TraverseBSTInOrderRecursive(BST * root) {
								if(root == NULL)   
																return;
								TraverseBSTInOrderRecursive(root->left);
								cout<<root->data<<"\t";
								TraverseBSTInOrderRecursive(root->right); 
}
/*http://algorithms.tutorialhorizon.com/check-if-one-binary-is-mirror-tree-of-another-binary-tree*/
bool IsMirror(BST * root1, BST * root2) {

if(root1 == NULL && root2 ==NULL)
   return true;

if(root1->data == root2->data)
   return true;


if((root1 == NULL && root2 != NULL) ||
(root2 == NULL && root1 != NULL)) 
   return false;

return IsMirror(root1->left, root2->right) && IsMirror(root1->right, root2->left); 

}


/*http://www.geeksforgeeks.org/symmetric-tree-tree-which-is-mirror-image-of-itself*/
bool Issymetric(BST * root) {

return IsMirror(root, root);

}

bool checkIsBST(BST * root, int min, int max) {
if(root == NULL) 
  return true;
 
if((root->data < max && root->data > min) && 
  checkIsBST(root->right, root->data, max) &&  
  checkIsBST(root->left, min, root->data))
  return true;

return false;

}

bool IsBST(BST * root) {
  return checkIsBST(root, INT_MIN, INT_MAX);
}


/*http://www.geeksforgeeks.org/sorted-linked-list-to-balanced-bst/ 

struct TNode* sortedListToBSTRecur(struct LNode **head_ref, int n); 
*/


